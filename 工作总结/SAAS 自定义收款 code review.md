# SAAS 自定义收款 code review

## 对自定义收款适配支持当前的收款退款逻辑
- 在  `SaasUnifiedPayAmountTypeEnum` 中新增了类型 `PAYMENT_CUSTOM_METHOD(-1, "代指所有用户自定义收款方式")`
- 调整了SaasUnifiedPayAmountTypeEnum.fromType 的方法
- 在统一支付的 biz层 配置文件`appcontext-core.xml`
- `IISaasUnifiedPaymentServiceImpl` 对退款流程修改，支付流程无需改动

## 对新门店的初始化
- 传递参数shopID 调用 `UnifiedPaymentMethodCustomDOServiceImpl#initShopPaymentMethod` 即可初始化
- `VirtualShopCreateSuccessListener` 监听虚拟门店的创建 并初始化
- `ShopCreateSuccessListener` 监听实体门店的创建 并排除掉共享门店

## 对老门店的初始
- `InitOldShopPaymentMethodJob` 初始化老门店 排除掉共享门店
- **存在一个问题 :**从门店那边获取的shopID 不包含status为0的门店 但存在门店从status = 0 -> status = 1

## 查询所有门店包含默认收款方式的键值对
- `ISaasUnifiedPaymentMethodCustomServiceImpl#findAllPaymentMethodMapByShopID`

## 查询时加入多门店逻辑
- `UnifiedPaymentMethodCustomDOServiceImpl#getQueryPaymentMethodShopID`
## 增删改加入多门店权限检测
- `ISaasUnifiedPaymentMethodCustomServiceImpl#checkShopIDUpdateOrInsertAuth`

## 在各个类型终端的web层为增加自定义收款方式的显示
- **saas-order-web :**`OrderController#populatePayment`为OrderVO 填充收款方式名称 (包含自定义收款方式) pad、app等也采用同等方式

